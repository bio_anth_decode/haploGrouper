HaploGrouper
========

A Python program for classifying VCF genotypes into haplogroups on the basis of a known phylogenetic tree.


## Requirements

HaploGrouper has been run successfully using Python 2.7, 3.6, and 3.12. It will likely work using similar versions.

It requires version ≥1.16.5 of the `numpy` library.


## Quick start

Please consult `UserGuide.pdf` for detailed instructions on using HaploGrouper.

Downloading HaploGrouper and running the help command:

```sh
# clone gitlab repository
git clone https://gitlab.com/bio_anth_decode/haploGrouper.git

# run HaploGrouper
cd haploGrouper/
python haploGrouper.py -h
```

```sh
usage: haploGrouper.py [-h] -v VCFFILE -o OUTFILE -l HGRPLOCUSFILE 
                       -t HGRPTREEFILE [-i IDLISTFILE] [-r REGIONS] [-c CHROM] 
                       [-f REFERENCEFASTA] [-m] [-w WEIGHTFILE] [-x VERBOSEFILE]

Assign haplogroups to samples based on haploid genotypes from a VCF file using 
a phylogeny defined by a locus file and tree file.

options:
  -h, --help            show this help message and exit
  -v VCFFILE, --vcfFile VCFFILE
                        Path of VCF file to be analysed. Can be gzipped.
  -o OUTFILE, --outFile OUTFILE
                        Path of output file
  -l HGRPLOCUSFILE, --hGrpLocusFile HGRPLOCUSFILE
                        Path of locus file associating mutations with branches
                        in the phylogeny
  -t HGRPTREEFILE, --hGrpTreeFile HGRPTREEFILE
                        Path of tree file describing branches in the phylogeny
  -i IDLISTFILE, --IDListFile IDLISTFILE
                        Path of file with newline-separated IDs from vcfFile
                        to assign haplogroups to
  -r REGIONS, --regions REGIONS
                        only base haplogroup assignment on positions from the
                        specified regions (format: startPos-stopPos,startPos-
                        stopPos). Multiple regions can be specified
  -c CHROM, --chrom CHROM
                        Only process loci in vcfFile on this chromosome. This
                        must be used when vcfFile contains loci from multiple
                        chromosomes
  -f REFERENCEFASTA, --referenceFasta REFERENCEFASTA
                        Read reference sequence from fasta file. This is
                        useful when the vcfFile is based on sequence data, but
                        only reports polymorphic positions or differences from
                        the reference sequence. In such cases, many
                        phylogenetically informative positions would be
                        ignored. When the reference sequence is provided,
                        positions not reported in vcfFile are assumed to have
                        the reference state for all individuals.
  -w WEIGHTFILE, --weightFile WEIGHTFILE
                        Give mutations differing weights read from a separate
                        file. This is only useful for loci with a high rate of
                        recurrent mutations - like the mtDNA control region.
                        The file should be tab-delimited with four columns:
                        pos ancAl derAl weight. Mutations not included in the
                        file default to a weight of 1
  -x VERBOSEFILE, --verboseFile VERBOSEFILE
                        Path of file describing full matrix of scores for each
                        node in the tree (lines) for each individual (columns)
  -m, --mismatchDetails
                        Add backMutLoci, thirdAlleleLoci, and otherDiff fields
                        to outFile
```

Below are some example commands. See `example_cmds.sh` for more.

## Example 1: running on a single individual to assign mitochondrial haplogroup

Mandatory parameters:
* `-v`: VCF genotype file - can be gzipped
* `-t`: tree file which describes the branches in the phylogeny
* `-l`: locus file which associates mutations with specific branches
* `-o`: output file

Optional parameters used here:
* `-f`: reference genome FASTA file used to fill in reference alleles for loci missing from VCF
* `-i`: new-line separated list of VCF file sample IDs to analyse - if omitted, all samples analysed
* `-x`: verbose output file showing scores for all nodes (haplogroups) for all samples

```sh
echo "HG00096" > docs/HG00096.txt 

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -i docs/HG00096.txt \
    -o docs/chrMT_HG00096_haplogroup.txt \
    -x docs/chrMT_HG00096_allScores.txt  
``` 
 
 
#### Output:

<table>
<thead>
<tr>
<th>ID</th>
<th>HaploGroup</th>
<th>netScore</th>
<th>matchScore</th>
<th>mismatchScore</th>
<th>mismatchLoci</th>
<th>backMutCnt</th>
<th>thirdAlleleCnt</th>
<th>pruning</th>
<th>allMaxNetScore</th>
<th>allMaxScore</th>
</tr>
</thead>
<tbody>
<tr>
<td>HG00096</td>
<td>H16a1</td>
<td>45</td>
<td>48</td>
<td>3</td>
<td>NA</td>
<td>3</td>
<td>0</td>
<td>NA</td>
<td>H16a1[48-3]</td>
<td>H16a1[48-3]</td>
</tr>
</tbody>
</table>


### Example 2: running on a single individual to assign mitochondrial haplogroup using the weight option

Optional parameters used here:
* `-w`: weight file used to assign different weights to loci

```sh
python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -w data/chrMT_phylotree17_weights.txt \
    -i docs/HG00096.txt \
    -o docs/chrMT_HG00096_weighted_haplogroup.txt \
    -x docs/chrMT_HG00096_weighted_allScores.txt
```

#### Output:

<table>
<thead>
<tr>
<th>ID</th>
<th>HaploGroup</th>
<th>netScore</th>
<th>matchScore</th>
<th>mismatchScore</th>
<th>mismatchLoci</th>
<th>backMutCnt</th>
<th>thirdAlleleCnt</th>
<th>pruning</th>
<th>allMaxNetScore</th>
<th>allMaxScore</th>
</tr>
</thead>
<tbody>
<tr>
<td>HG00096</td>
<td>H16a1</td>
<td>437</td>
<td>460.1</td>
<td>23.1</td>
<td>NA</td>
<td>3</td>
<td>0</td>
<td>NA</td>
<td>H16a1[460.1-23.1]</td>
<td>H16a1[460.1-23.1]</td>
</tr>
</tbody>
</table>


### Example 3: running on a single individual to assign Y-chromosome haplogroup using deCODE fork of ISOGG 2019 files

```sh
python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -i docs/HG00096.txt \
    -o docs/chrY_isogg2019-decode1_HG00096_haplogroup.txt \
    -x docs/chrY_isogg2019-decode1_HG00096_allScores.txt
```

#### Output:

<table>
<thead>
<tr>
<th>ID</th>
<th>HaploGroup</th>
<th>netScore</th>
<th>matchScore</th>
<th>mismatchScore</th>
<th>mismatchLoci</th>
<th>backMutCnt</th>
<th>thirdAlleleCnt</th>
<th>pruning</th>
<th>allMaxNetScore</th>
<th>allMaxScore</th>
</tr>
</thead>
<tbody>
<tr>
<td>HG00096</td>
<td>R1b1a1b1a1a2c1a1f1</td>
<td>952</td>
<td>952</td>
<td>0</td>
<td>NA</td>
<td>0</td>
<td>1</td>
<td>NA</td>
<td>R1b1a1b1a1a2c1a1f1[952-0]</td>
<td>R1b1a1b1a1a2c1a1f1[952-0]</td>
</tr>
</tbody>
</table>


### Example 4: running on a single individual to assign Y-chromosome haplogroup using ISOGG 2019 files, showing mismatch details

Optional parameters used here:
* `-m`: flag which adds additional fields describing mismatches to outFile

```sh
python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -i docs/HG00096.txt \
    -m \
    -o docs/chrY_isogg2019-decode1_HG00096_haplogroup_with_details.txt
```

#### Output:

<table>
<thead>
<tr>
<th>ID</th>
<th>HaploGroup</th>
<th>netScore</th>
<th>matchScore</th>
<th>mismatchScore</th>
<th>mismatchLoci</th>
<th>backMutCnt</th>
<th>backMutLoci</th>
<th>thirdAlleleCnt</th>
<th>thirdAlleleLoci</th>
<th>otherDiff</th>
<th>pruning</th>
<th>allMaxNetScore</th>
<th>allMaxScore</th>
</tr>
</thead>
<tbody>
<tr>
<td>HG00096</td>
<td>R1b1a1b1a1a2c1a1f1</td>
<td>952</td>
<td>952</td>
<td>0</td>
<td>NA</td>
<td>0</td>
<td>NA</td>
<td>1</td>
<td>N[P1 [K2b2a]]A2842214T</td>
<td>0</td>
<td>NA</td>
<td>R1b1a1b1a1a2c1a1f1[952-0]</td>
<td>R1b1a1b1a1a2c1a1f1[952-0]</td>
</tr>
</tbody>
</table>


## isogg2019-decode1 fork

With HaploGrouper v1.1, we released a forked version of the ISOGG 2019 14.196 Y-chromosome locus file. We recommend using this isogg2019-decode1 locus file instead of the original isogg2019 locus file as it can lead to substantially better resolved haplogroup assignments in some use cases. See `data/chrY_isogg2019-decode1_loci_README.txt` for details.


## Directories

`data/`: locus and tree files generated by deCODE, plus example 1000G data used as input for the example commands.

`docs/`: output generated by the example commands.

`paper/`: supplementary files generated for the Bioinformatics article presenting HaploGrouper.


## Support

If you experience problems using HaploGrouper, please [report an issue on the GitLab repository here](https://gitlab.com/bio_anth_decode/haploGrouper/-/issues).

If you cannot use the GitLab issues tracker for whatever reason, you can also email `agnar [at-symbol] decode [dot] is` and `kristjanm [at-symbol] decode [dot] is`.


## Citing HaploGrouper

Please cite the [Bioinformatics paper](https://academic.oup.com/bioinformatics/article/37/4/570/5893552) if you use HaploGrouper in your study:

Jagadeesan, A. et al. HaploGrouper: a generalized approach to haplogroup classification. Bioinformatics vol. 37 570–572 (2020)

