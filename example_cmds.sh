
################## from README: running on a single individual #################

echo "HG00096" > docs/HG00096.txt

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -i docs/HG00096.txt \
    -o docs/chrMT_HG00096_haplogroup.txt \
    -x docs/chrMT_HG00096_allScores.txt

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -w data/chrMT_phylotree17_weights.txt \
    -i docs/HG00096.txt \
    -o docs/chrMT_HG00096_weighted_haplogroup.txt \
    -x docs/chrMT_HG00096_weighted_allScores.txt

python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -i docs/HG00096.txt \
    -o docs/chrY_isogg2019-decode1_HG00096_haplogroup.txt \
    -x docs/chrY_isogg2019-decode1_HG00096_allScores.txt

python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -i docs/HG00096.txt \
    -m \
    -o docs/chrY_isogg2019-decode1_HG00096_haplogroup_with_details.txt


######################## mitochondrial complete sequence #######################

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -o docs/chrMT_1000G_phase3_haplogroup.txt \
    -x docs/chrMT_1000G_phase3_allScores.txt


######################### mitochondrial control region #########################

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -f data/rCRS.fasta \
    -r 0-600,16024-16365 \
    -o docs/chrMT_1000G_phase3_controlRegion_haplogroup.txt \
    -x docs/chrMT_1000G_phase3_controlRegion_allScores.txt


################### mitochondrial control region with weights ##################

python haploGrouper.py \
    -v data/ALL.chrMT.phase3_callmom-v0_4.20130502.genotypes.vcf.gz \
    -t data/chrMT_phylotree17_tree.txt \
    -l data/chrMT_phylotree17_loci.txt \
    -w data/chrMT_phylotree17_weights.txt \
    -f data/rCRS.fasta \
    -r 0-600,16024-16365 \
    -o docs/chrMT_1000G_phase3_controlRegion_weighted_haplogroup.txt \
    -x docs/chrMT_1000G_phase3_controlRegion_weighted_allScores.txt


######## Y chromosome using ISOGG 2019 with deCODE fixes (recommended) #########

python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -o docs/chrY_isogg2019-decode1_1000G_haplogroup.txt \
    -x docs/chrY_isogg2019-decode1_1000G_allScores.txt


########## Y chromosome using ISOGG 2019 with deCODE fixes, build 38 ###########

python haploGrouper.py \
    -v data/CCDG_14151_B01_GRM_WGS_2020-08-05_chrY.recalibrated_variants.3_samples.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b38.txt \
    -o docs/chrY_isogg2019-decode1_subset1000G_haplogroup_b38.txt \
    -x docs/chrY_isogg2019-decode1_subset1000G_allScores_b38.txt


############## Y chromosome using ISOGG 2019 without deCODE fixes ##############

python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019_loci_b37.txt \
    -o docs/chrY_isogg2019_1000G_haplogroup.txt \
    -x docs/chrY_isogg2019_1000G_allScores.txt


###### Y chromosome using ISOGG 2019 with deCODE fixes, mismatch details #######

python haploGrouper.py \
    -v data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf.gz \
    -t data/chrY_isogg2019_tree.txt \
    -l data/chrY_isogg2019-decode1_loci_b37.txt \
    -m \
    -o docs/chrY_isogg2019-decode1_1000G_haplogroup_with_details.txt
