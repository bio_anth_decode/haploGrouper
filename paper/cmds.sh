
python ../hGrpr2.py -v ../data/ALL.chrY.phase3_integrated_v2a_GSA_Y.recode.vcf \
                    -t ../data/chrY_isogg2016-deprecated_tree.txt \
                    -l ../data/chrY_isogg2016-deprecated_loci_b37.txt \
                    -o chrY_isogg2016_b37_1000G_gsa_haplogroup.txt \
                    -x chrY_isogg2016_b37_1000G_gsa_allScores.txt

python ../hGrpr2.py -v ../data/ALL.chrY.phase3_integrated_v2a.20130502.genotypes.vcf \
                    -t ../data/chrY_isogg2016-deprecated_tree.txt \
                    -l ../data/chrY_isogg2016-deprecated_loci_b37.txt \
                    -o chrY_isogg2016_b37_1000G_haplogroup.txt \
                    -x chrY_isogg2016_b37_1000G_allScores.txt

python ../hGrpr2.py -v ../data/ALL.chrMT.phase1_samtools_si.20101123.snps.low_coverage.genotypes.vcf \
                    -t ../data/chrMT_phylotree17_tree.txt \
                    -l ../data/chrMT_phylotree17_loci.txt \
                    -f ../data/rCRS.fasta \
                    -o chrMT_1000G_phase1_haplogroups.txt \
                    -x chrMT_1000G_phase1_allScores.txt
